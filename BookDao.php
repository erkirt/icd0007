<?php
require_once 'Book.php';

class BookDao
{
    function getConnection(){

        $user = "erkirt";
        $pass = "df6a2d";

        $address = "mysql:host=db.mkalmo.xyz;dbname=erkirt";

        return new PDO($address, $user, $pass,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }
    function getAllBooks(){
        $conn = $this->getConnection();
        $statement = $conn->prepare("select Raamatud.book_id, Raamatud.title, Raamatud.grade, Raamatud.authorID,Raamatud.isRead, author.author_id, author.firstName, author.lastName FROM Raamatud 
                                 left join author on Raamatud.authorID = author.author_id");
        $statement->execute();

        $books = [];

        foreach ($statement as $row){
            $books[] = new Book($row['book_id'], urldecode($row['title']),$row['authorID'], urldecode($row['firstName']), urldecode($row['lastName']),$row['grade'],$row['isRead']);}
        return $books;
        // vb on author_id mitte authorID
    }
    function addBook($book){
        $conn = $this->getConnection();
        $statement = $conn->prepare('insert into Raamatud(title, authorID, grade, isRead) values (:title, :author, :grade, :isRead)');
        $statement->bindValue(':title', urlencode($book->title));
        $statement->bindValue(':author', $book->authorID);
        $statement->bindValue(':grade', $book->grade);
        $statement->bindValue(':isRead', $book->isRead);
        $statement->execute();
    }
    function editBook($editedBook){
        $conn = $this->getConnection();
        $statement = $conn->prepare("update Raamatud set title = :title, grade = :grade, isRead = :isRead, authorID = :authorID where book_id = :book_id");
        $statement->bindValue(":book_id", $editedBook->book_id);
        $statement->bindValue(':grade', $editedBook->grade);
        $statement->bindValue(':title', urlencode($editedBook->title));
        $statement->bindValue(':isRead', $editedBook->isRead);
        $statement->bindValue(':authorID', $editedBook->authorID);
        $statement->execute();
    }
    function deleteBook($book_id){
        $conn = $this->getConnection();
        $statement = $conn->prepare("delete from Raamatud where book_id = :id");
        $statement->bindValue(':id', $book_id);
        $statement->execute();
    }
    function BookById($book_id){
        $conn = $this->getConnection();
        $statement = $conn->prepare("SELECT Raamatud.book_id, Raamatud.title, Raamatud.grade, Raamatud.authorID, Raamatud.isRead, author.author_id, author.firstName, author.lastName FROM Raamatud 
                                 left join author on Raamatud.authorID = author.author_id where Raamatud.book_id = $book_id");
        $statement -> execute();
        $foundBook = '';
        foreach ($statement as $row){
            $foundBook = new Book($row['book_id'], urldecode($row['title']),$row['authorID'], urlencode($row['firstName']), urlencode($row['lastName']),$row['grade'],$row['isRead']);}
        return $foundBook;
    }
    function isOk($book){
        $error = [];
        if (3 > strlen($book -> title) || strlen($book -> title) > 23) {
            $error[] = "Title needs to be 3 to 23 characters long" . ", yours is: " . strlen($book -> title);
        }
        return $error;
    }
}