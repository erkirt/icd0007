<?php
require_once('vendor/tpl.php');
require_once('BookDao.php');
require_once('AuthorDao.php');
require_once('Request.php');
require_once('Book.php');
require_once('Author.php');


$request = new Request($_REQUEST);

$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'book-list';
$authors = new AuthorDao();
$books = new BookDao();

if ($cmd === 'book-list'){
    $data = [
        'message' => $request->param('message'),
        'books' => $books->getAllBooks(),
        'template' => 'book-list.html'
    ];
    print renderTemplate('main.html', $data);
}
else if ($cmd === 'book-add'){

    $data = [
        'authors' => $authors->getAllAuthors(),
        'template' => 'book-add.html',
        'book' => new Book(0, "", 0, "", "", 0, "")
    ];
    print renderTemplate('main.html', $data);
}
else if (($request->param("cmd") == "save-book")) {
    $book1 = getBookItems();
    $errors = $books ->isOk($book1);
    if (empty($errors)){
        $books->addBook($book1);
        header('Location:index.php?cmd=book-list&message=Added!');
    }
    else {
        $data = [
            'authors' => $authors->getAllAuthors(),
            'errors' => $errors,
            'book' => $book1,
            'template' => 'book-add.html'
        ];
        print renderTemplate('main.html', $data);
    }
}
else if ($cmd === 'editBook'){

    $data = [
        'authors' => $authors->getAllAuthors(),
        'book' => $books->BookById($request->param("book_id")),
        'template' => 'book-delete.html'
    ];
    print renderTemplate('main.html', $data);}

else if ($request->param("cmd") == "edit-book"){
    $book2 = getBookItems();
    $errors2 = $books ->isOk($book2);
    if ($request->param("deleteButton")){
        $books ->deleteBook($request->param($book2 ->getID()));
        header('Location: index.php?cmd=book-list&message=Deleted!');}
    else if (empty($errors2)){
        $books ->editBook($book2);
        header('Location: index.php?cmd=book-list&message=Updated!');}

    else{
        $data = [
            'book' => $book2,
            'errors' => $errors2,
            'template' => 'book-delete.html'
        ];
        print renderTemplate('main.html', $data);
    }
}












else if ($cmd === 'author-list'){

    $data = [
        'message' => $request->param('message'),
        'authors' => $authors->getAllAuthors(),
        'template' => 'author-list.html'
    ];

    print renderTemplate('main.html', $data);
}
else if ($cmd === 'author-add') {
    $data = [
        'template' => 'author-add.html',
        'author' => new Author(0, "", "", 0),
    ];
    print renderTemplate('main.html', $data);
}
else if (($request->param("cmd") == "save-author")) {
    $author1 = getAuthorItems();
    $errors1 = $authors ->isOk($author1);
    if (empty($errors1)){
        $authors ->addAuthor($author1);
        header('Location: index.php?cmd=author-list&message=Added!');
    }
    else {
        $data = [
            'author' => $author1,
            'errors' => $errors1,
            'template' => 'author-add.html'
        ];
        print renderTemplate('main.html', $data);
    }
}
else if ($cmd === 'edit_author'){
    $data = [
        'author' => $authors->authorById($request->param("author-id")),
        'template' => 'author-delete.html'
    ];
    print renderTemplate('main.html', $data);}

else if (($request->param("cmd") == "edit-author")){
        $author1 = getAuthorItems();
        $errors1 = $authors ->isOk($author1);
        if ($request->param("deleteButton")){
        $authors->deleteauth($request->param('id'));
            header('Location: index.php?cmd=author-list&message=Deleted!');
    }
        else if (empty($errors1) && $request->param("submitButton")){
            $authors ->editAuth($author1);
            header('Location: index.php?cmd=author-list&message=Updated!');
        }
        else{
            $data = [
                'author' => $author1,
                'errors' => $errors1,
                'template' => 'author-delete.html'
            ];
            print renderTemplate('main.html', $data);
    }
}





function getBookItems(): Book
{
    $request = new Request($_REQUEST);
    $id = $request->param('book_id');
    $title = $request->param('title');
    $authorID = $request->param('author1');
    $firstName = $request->param("firstName");
    $lastName = $request->param("lastName");
    $grade = $request->param("grade");
    $isRead = $request->param("isRead");
    return new Book($id, $title, $authorID, $firstName, $lastName, $grade, $isRead);
}

function getAuthorItems(): Author{
    $request = new Request($_REQUEST);
    $author_id = $request->param('id');
    $firstName = $request->param("firstName");
    $lastName = $request->param("lastName");
    $grade = $request->param("grade");
    return new Author($author_id, $firstName, $lastName, $grade);
}

