<?php
require_once 'Author.php';

class AuthorDao
{
    function getConnection(){

        $user = "erkirt";
        $pass = "df6a2d";

        $address = "mysql:host=db.mkalmo.xyz;dbname=erkirt";

        return new PDO($address, $user, $pass,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    function getAllAuthors(){
        $conn = $this->getConnection();
        $statement = $conn->prepare("SELECT author_id, firstName, lastName, grade FROM author ");
        $statement->execute();

        $authors = [];

        foreach ($statement as $author) {
            $authors[] = new Author($author["author_id"], urldecode($author["firstName"]), urldecode($author["lastName"]), $author["grade"]);
        }

        return $authors;
    }

    function addAuthor($author){
        $conn = $this->getConnection();
        $statement = $conn->prepare('insert into author(firstName, lastName, grade) values (:firstName, :lastName, :grade)');
        $statement->bindValue(':firstName', urlencode($author->firstName));
        $statement->bindValue(':lastName', urlencode($author->lastName));
        $statement->bindValue(':grade', $author->grade);
        $statement->execute();
    }

    function editAuth($editedAuthor){
        $conn = $this->getConnection();
        $statement = $conn->prepare("update author set firstName = :firstName, lastName = :lastName, grade = :grade where author_id = :author_id");
        $statement->bindValue(":firstName", urlencode($editedAuthor->firstName));
        $statement->bindValue(":lastName", urlencode($editedAuthor->lastName));
        $statement->bindValue(':grade', $editedAuthor->grade);
        $statement->bindValue(":author_id", $editedAuthor->author_id);
        $statement->execute();
    }
    function deleteauth($author_id){
        $conn = $this->getConnection();
        $statement = $conn->prepare("delete from author where author_id = :id");
        $statement->bindValue(':id', $author_id);
        $statement->execute();
    }
    function authorById($author_id){
        $conn = $this->getConnection();
        $statement = $conn -> prepare("select author_id, firstName, lastName, grade from author where author_id = '$author_id'");
        $statement -> execute();
        $author =$statement->fetch();
        return new Author($author["author_id"], urldecode($author["firstName"]), urldecode($author["lastName"]), $author["grade"]);
    }
    function isOk($author){
        $firstName = $author->firstName;
        $lastName = $author->lastName;
        $error = [];


        if (1 > strlen($firstName) || strlen($firstName) > 21){
            $error[] = "Author first name needs to be 1 to 21 characters long" . ", yours is: " . strlen($firstName);}
        if (2 > strlen($lastName) || strlen($lastName) > 22){
            $error[] = "Author last name needs to be 2 to 22 characters long" . ", yours is: " . strlen($lastName);}

        return $error;
    }
}