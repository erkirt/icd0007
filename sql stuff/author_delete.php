<?php
require_once("functions.php");
$error = [];
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $author_id = $_GET['author_id'];
    $author = authorById($author_id);
    $firstName = $author["firstName"];
    $lastName = $author["lastName"];
    $grade = $author["grade"];
    $error = [];

}
//if ($_SERVER["REQUEST_METHOD"] == "POST") {
else{
    $firstName = $_POST["firstName"] ?? "";
    $lastName = $_POST["lastName"] ?? "";
    $grade = $_POST["grade"] ?? "";
    $author_id = $_POST["author_id"] ?? "";
    $edit = $_POST["submitButton"] ?? "";
    $deletens = $_POST["deleteButton"] ?? "";
    if (1 > strlen($firstName) || strlen($firstName) > 21){
        $error[] = "Author first name needs to be 1 to 21 characters long" . ", yours is: " . strlen($firstName);}
    if (2 > strlen($lastName) || strlen($lastName) > 22){
        $error[] = "Author last name needs to be 2 to 22 characters long" . ", yours is: " . strlen($lastName);}
    if (empty($error)) {
        if($deletens == 'Kustuta'){
            deleteauth($author_id);
            header("Location: author-list.php?Message=deleted!");
        }
        else if($edit == 'Salvesta'){
            editAuth($author_id,$firstName,$lastName,$grade);
            header("Location: author-list.php?Message=updated!");
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../kujundus.css">
    <meta charset="UTF-8">
    <title>Author Add</title>
</head>
<nav>
    <a href="index-old.php" id="book-list-link">Raamatud</a>
    <a href="book-add.php" id="book-form-link">Lisa Raamat</a>
    <a href="author-list.php" id="author-list-link">Autorid</a>
    <a href="author-add.php" id="author-form-link">Lisa Autor</a>
</nav>
<body id="author-delete">
<form id="input_form" action="author_delete.php" method="post">
    <input type="hidden" id="author_id" name="author_id" value="<?=$author_id?>">
    <table class="content-table">
        <?php if (!empty($error)){
            foreach ($error as $currentError){
                echo "<ul id='error-block' class='alert'>" . $currentError . "</ul>";
            }
        }?>
        <thead>
        </thead>
        <tbody>
        <tr>
            <td class="first_child"><label for="firstName">Eesnimi: </label></td>
            <td><input id="firstName" type="text" name="firstName" value="<?=$firstName?>"></td>
        </tr>
        <tr>
            <td class="first_child"><label for="lastName">Perekonnanimi: </label></td>
            <td>
                <input type="text" id="lastName" name="lastName" value="<?= $lastName?>">
            </td>
        </tr>
        <tr>
            <td class="first_child"><div class="label-cell">Hinne: </div>
            </td>
            <td>
                <div class="input-cell">
                    <label>
                        <?php for ($i = 1; $i <= 5; $i++):?>
                            <?php if (($grade == $i)): ?>
                                <input type="radio" name="grade" value="<?=$i?>" checked><?=$i?>
                            <?php else: ?>
                                <input type="radio" name="grade" value="<?=$i?>"><?=$i?>
                            <?php endif;?>
                        <?php endfor ?>
                    </label>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <input type="submit" id="submitButton" name="submitButton" value="Salvesta">
    <input type="submit" id="deleteButton" name="deleteButton" value="Kustuta" >
</form>
<footer>ICD0007 Ergo Kirt</footer>
</body>
</html>