<?php
function getAllAuthors(){
    $conn = getConnectsion();
    $statement = $conn->prepare("SELECT author_id, firstName, lastName, grade FROM author ");
    $statement->execute();

    $authors = [];

    foreach ($statement as $row){
        $authors[] = ['author_id' => $row['author_id'], 'firstName' => urldecode($row['firstName']),
            'lastName' => urldecode($row['lastName']), 'grade' => $row['grade']];
    }
    return $authors;
}
function addAuthor($firstName, $lastName, $grade){
    $conn = getConnectsion();
    $statement = $conn->prepare('insert into author(firstName, lastName, grade) values (:firstName, :lastName, :grade)');
    $statement->bindValue(':firstName', urlencode($firstName));
    $statement->bindValue(':lastName', urlencode($lastName));
    if (!in_array($grade, [1,2,3,4,5])){
        $statement->bindValue(':grade', 0);}
    else {
        $statement->bindValue(':grade', $grade);}
    $statement->execute();
}
function addBook($title, $authorID, $grade, $isRead){
    $conn = getConnectsion();
    $statement = $conn->prepare('insert into Raamatud(title, authorID, grade, isRead) values (:title, :author, :grade, :isRead)');
    $statement->bindValue(':title', urlencode($title));
    $statement->bindValue(':author', $authorID);
    if (!in_array($grade, [1,2,3,4,5])){
        $statement->bindValue(':grade', 0);}
    else {
        $statement->bindValue(':grade', $grade);}
    $statement->bindValue(':isRead', $isRead);
    $statement->execute();
}

function editAuth($author_id, $firstName, $lastName, $grade){
    $conn = getConnectsion();
    $statement = $conn->prepare("update author set firstName = :firstName, lastName = :lastName, grade = :grade
            where author_id = :author_id");
    $statement->bindValue(":firstName", urlencode($firstName));
    $statement->bindValue(":lastName", urlencode($lastName));
    if (!in_array($grade, [1,2,3,4,5])){
        $statement->bindValue(':grade', 0);}
    else {
        $statement->bindValue(':grade', $grade);}
    $statement->bindValue(":author_id", $author_id);
    $statement->execute();
}
function deleteauth($author_id){
    $conn = getConnectsion();
    $statement = $conn->prepare("delete from author where author_id = :id");
    $statement->bindValue(':id', $author_id);
    $statement->execute();
}
function authorById($author_id){
    $conn = getConnectsion();
    $statement = $conn -> prepare("select author_id, firstName, lastName, grade from author where author_id = '$author_id'");
    $statement -> execute();
    $foundAuthor = '';
    foreach ($statement as $row){
        $foundAuthor = ['firstName' => urldecode($row['firstName']), 'lastName' => urldecode($row['lastName']),
            'grade' => $row['grade']];
    }
    return $foundAuthor;
}
function getAllBooks(){
    $conn = getConnectsion();
    $statement = $conn->prepare("SELECT Raamatud.book_id, Raamatud.title, Raamatud.grade, Raamatud.authorID,Raamatud.isRead, author.author_id, author.firstName, author.lastName FROM Raamatud 
                                 left join author on Raamatud.authorID = author.author_id");
    $statement->execute();

    $books = [];

    foreach ($statement as $row){
        $books[] = ['book_id' => $row['book_id'], 'title' => urldecode($row['title']),
            'author_id' => $row['author_id'], 'firstName' => urldecode($row['firstName']),
            'lastName' => urldecode($row['lastName']), 'grade' => $row['grade'],
            'isRead' => $row['isRead'], 'authorID' => $row['authorID']];
    }
    return $books;
}
function editBook($book_id, $title, $grade, $isRead, $authorID){
    $conn = getConnectsion();
    $statement = $conn->prepare("update Raamatud set title = :title, grade = :grade, isRead = :isRead, authorID = :authorID
            where book_id = :book_id");
    $statement->bindValue(":book_id", $book_id);
    if (!in_array($grade, [1,2,3,4,5])){
        $statement->bindValue(':grade', 0);}
    else {
        $statement->bindValue(':grade', $grade);}
    $statement->bindValue(":title", urlencode($title));
    $statement->bindValue(":isRead", $isRead);
    $statement->bindValue(":authorID", $authorID);
    $statement->execute();
}
function deleteBook($book_id){
    $conn = getConnectsion();
    $statement = $conn->prepare("delete from Raamatud where book_id = :id");
    $statement->bindValue(':id', $book_id);
    $statement->execute();
}
function BookById($book_id){
    $conn = getConnectsion();
    $statement = $conn->prepare("SELECT Raamatud.book_id, Raamatud.title, Raamatud.grade, Raamatud.authorID, Raamatud.isRead, author.author_id, author.firstName, author.lastName FROM Raamatud 
                                 left join author on Raamatud.authorID = author.author_id where Raamatud.book_id = $book_id");
    $statement -> execute();
    $foundBook = '';
    foreach ($statement as $row){
        $foundBook = ['book_id' => $row['book_id'], 'title' => urldecode($row['title']),
            'authorID' => $row['authorID'], 'firstName' => urldecode($row['firstName']),
            'lastName' => urldecode($row['lastName']), 'grade' => $row['grade'], 'isRead' => $row['isRead']];
    }
    return $foundBook;
}
function getConnectsion(){

    $user = "erkirt";
    $pass = "df6a2d";

    $address = "mysql:host=db.mkalmo.xyz;dbname=erkirt";

    return new PDO($address, $user, $pass,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
