<?php
require_once("functions.php");
$books = getAllBooks();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../kujundus.css">
    <meta charset="UTF-8">
    <title>Book List</title>
</head>
<body id="book-list-page">
<nav>
    <a href="index-old.php" id="book-list-link">Ramatud</a>
    <a href="book-add.php" id="book-form-link">Lisa Raamat</a>
    <a href="author-list.php" id="author-list-link">Autorid</a>
    <a href="author-add.php" id="author-form-link">Lisa Autor</a>
</nav>
<main>
    <?php if (isset($_GET["Message"])){
        echo "<ul id='message-block'>" . $_GET['Message'] . "</ul>";
    }?>
    <table class="content-table">
        <thead>
        <tr>
            <th>Pealkiri</th>
            <th>Autorid</th>
            <th>Hinne</th>
        </tr>
        </thead>
        <tbody class="colorful">
        <?php foreach ($books as $book): ?>
            <tr>
                <td><a href="book_delete.php?book_id=<?=$book['book_id']?>"><?=$book["title"]?></a></td>
                <?php if (empty($book["authorID"])){
                    echo "<td></td>";}
                else {echo "<td>" .$book["firstName"]. " " .$book["lastName"]. "</td>";}?>

                <td><?=$book["grade"]?></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</main>
<footer>ICD0007 Ergo Kirt</footer>
</body>
</html>