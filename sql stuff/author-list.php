<?php
require_once("functions.php");
$allauthors = getAllAuthors();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../kujundus.css">
    <meta charset="UTF-8">
    <title>Author List</title>
</head>
<body id="author-list-page">
<nav>
    <a href="index-old.php" id="book-list-link">Raamatud</a>
    <a href="book-add.php" id="book-form-link">Lisa Raamat</a>
    <a href="author-list.php" id="author-list-link">Autorid</a>
    <a href="author-add.php" id="author-form-link">Lisa Autor</a>
</nav>
<main>
    <?php
    if (isset($_GET["Message"])){
        echo "<ul id='message-block' class='alert'>" . $_GET['Message'] . "</ul>";
    }?>
    <table class="content-table">
        <thead>
        <tr id="authors">
            <th>Nimi</th>
            <th>Perekonnanimi</th>
            <th>Hinne</th>
        </tr>
        </thead>
        <tbody class="colorful">
        <?php foreach ($allauthors as $author):?>
            <tr>
                <td><a href="author_delete.php?author_id=<?=$author['author_id']?>"><?= $author["firstName"]?></a></td>
                <td><?= $author["lastName"]?></td>
                <td><?= $author["grade"]?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</main>
<footer>ICD0007 Ergo Kirt</footer>
</body>
</html>