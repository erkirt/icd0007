<?php
require_once("functions.php");
$error = [];
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $book_id = $_GET['book_id'];
    $book = BookById($book_id);
    $authorID = $book['authorID'];
    $title = $book['title'];
    $author = $book['firstName'] . " " . $book['lastName'];
    $grade = $book['grade'];
    $isRead = $book['isRead'];
    $authors = getAllAuthors();
} else {
    $title = $_POST["title"] ?? "";
    $authorID = $_POST["author1"];
    $book_id = $_POST["book_id"] ?? "";
    $grade = $_POST["grade"] ?? "";
    $isRead = $_POST["isRead"] ?? "";
    $edit = $_POST["submitButton"] ?? "";
    $deletens = $_POST["deleteButton"] ?? "";
    if (3 > strlen($title) || strlen($title) > 23) {
        $error[] = "Title needs to be 3 to 23 characters long" . ", yours is: " . strlen($title);
    }
    if (empty($title)) {
        $error[] = "Title is missing";
    }
    if (empty($error)) {
        if ($deletens == 'Kustuta') {
            deleteBook($book_id);
            header("Location: index-old.php?Message=deleted!");
        } else if ($edit == 'Salvesta') {
            editBook($book_id, $title, $grade, $isRead, $authorID);
            header("Location: index-old.php?Message=updated!");
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../kujundus.css">
    <meta charset="UTF-8">
    <title>Book Add</title>
</head>
<body id="book-delete">
<nav>
    <a href="index-old.php" id="book-list-link">Raamatud</a>
    <a href="book-add.php" id="book-form-link">Lisa Raamat</a>
    <a href="author-list.php" id="author-list-link">Autorid</a>
    <a href="author-add.php" id="author-form-link">Lisa Autor</a>
</nav>
<form id="input_form" action="book_delete.php" method="post">
    <input type="hidden" id="book_id" name="book_id" value="<?= $book_id ?>">
    <table class="content-table">
        <?php if (!empty($error)) {
            foreach ($error as $currentError) {
                echo "<ul id='error-block' class='alert'>" . $currentError . "</ul>";
            }
        }?>
        <tbody>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="title">Pealkiri:</label></div>
            </td>
            <td>
                <div class="input-cell"><input id="title" name="title" type="text" value="<?= $title ?>"></div>
            </td>
        </tr>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="author">Autor:</label></div>
            </td>
            <td>
                <div>
                    <label for="author1"></label><select name="author1" id="author1">
                        <?php if ($authorID === 0) {
                            echo "<option value='" . $authorID . "'>" . "</option>";
                        } ?>
                        <option value="<?= $authorID ?>"><?= $author ?></option>
                        <?php foreach ($authors as $info): ?>
                            <?php if ((($info['firstName'] . " " . $info['lastName']) != $author)): ?>
                                <option value="<?= $info['author_id'] ?>"><?= $info['firstName'] . " " . $info['lastName'] ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="author2">Autor2:</label></div>
            </td>
            <td><label for="author2"></label><select name="author2" id="author2"></select></td>
        </tr>
        <tr>
            <td class="first_child"><div class="label-cell">Hinne: </div>
            </td>
            <td>
                <div class="input-cell">
                    <label>
                        <?php for ($i = 1; $i <= 5; $i++):?>
                            <?php if (($grade == $i)): ?>
                                <input type="radio" name="grade" value="<?=$i?>" checked><?=$i?>
                            <?php else: ?>
                                <input type="radio" name="grade" value="<?=$i?>"><?=$i?>
                            <?php endif;?>
                        <?php endfor ?>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="isRead">Loetud:</label></div>
            </td>
            <td>
                <div class="input-cell">
                    <label>
                        <?php if (!empty($isRead)): ?>
                            <input id="isRead" name="isRead" type="checkbox" checked>
                        <?php else: ?>
                            <input id="isRead" name="isRead" type="checkbox"/>
                        <?php endif;?>
                    </label>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <input type="submit" id="submitButton" name="submitButton" value="Salvesta">
    <input type="submit" id="deleteButton" name="deleteButton" value="Kustuta">
</form>
<footer>ICD0007 Ergo Kirt</footer>
</body>
</html>