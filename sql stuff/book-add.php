<?php
require_once("functions.php");
$authorFirst = getAllAuthors();
$title = '';
$author = '';
$grade = '';
$isRead = '';
$error = [];

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $title = $_POST["title"] ?? "";
    $authorID = $_POST["author1"];
    $grade = $_POST["grade"] ?? "";
    $isRead = $_POST["isRead"] ?? "";
    if (3 > strlen($title) || strlen($title) > 23) {
        $error[] = "Title needs to be 3 to 23 characters long" . ", yours is: " . strlen($title);
    }
    if (empty($error)) {
        addBook($title, $authorID, $grade, $isRead);
        header("Location: index-old.php?Message=Successssss!");
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../kujundus.css">
    <meta charset="UTF-8">
    <title>Book Add</title>
</head>
<body id='book-form-page'>
<nav>
    <a href="index-old.php" id="book-list-link">Raamatud</a>
    <a href="book-add.php" id="book-form-link">Lisa Raamat</a>
    <a href="author-list.php" id="author-list-link">Autorid</a>
    <a href="author-add.php" id="author-form-link">Lisa Autor</a>
</nav>
<form id="input_form" action="book-add.php" method="post">
    <table class="content-table">
        <?php if (!empty($error)) {
            foreach ($error as $currentError) {
                echo "<ul id='error-block' class='alert'>" . $currentError . "</ul>";
            }
        } ?>
        <tbody>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="title">Pealkiri:</label></div>
            </td>
            <td>
                <div class="input-cell"><input id="title" name="title" type="text" value="<?= $title ?>"></div>
            </td>
        </tr>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="author1">Autor1:</label></div>
            </td>
            <td>
                <label for="author1"></label>
                <select name="author1" id="author1">
                    <option value=0></option>
                    <?php foreach ($authorFirst as $parts): ?>
                        <option value="<?=$parts['author_id']?>"><?= $parts["firstName"] ?> <?= $parts["lastName"] ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="author2">Autor2:</label></div>
            </td>
            <td><label for="author2"></label><select name="author2" id="author2"></select></td>
        </tr>
        <tr>
            <td class="first_child"><div class="label-cell">Hinne: </div>
            </td>
            <td>
                <div class="input-cell">
                    <label>
                        <?php for ($i = 1; $i <= 5; $i++):?>
                            <?php if (($grade == $i)): ?>
                                <input type="radio" name="grade" value="<?=$i?>" checked><?=$i?>
                            <?php else: ?>
                                <input type="radio" name="grade" value="<?=$i?>"><?=$i?>
                            <?php endif;?>
                        <?php endfor ?>
                    </label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="first_child">
                <div class="label-cell"><label for="isRead">Loetud:</label></div>
            </td>
            <td>
                <div class="input-cell">
                <label>
                        <?php if (!empty($isRead)): ?>
                            <input id="isRead" name="isRead" type="checkbox" checked>
                        <?php else: ?>
                            <input id="isRead" name="isRead" type="checkbox"/>
                        <?php endif;?>
                </label>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <input type="submit" id="submitButton" name="submitButton" value="Salvesta">
</form>
<footer>ICD0007 Ergo Kirt</footer>
</body>
</html>