<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];


var_dump(is_in_list($numbers, 1));
var_dump(is_in_list($numbers, 9));

function is_in_list($list, $needle) {

    foreach ($list as $each) {
        if ($each === $needle) {
            return true;
        }
    }

    return false;
}