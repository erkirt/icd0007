<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];


print_r(get_odd_numbers($numbers));

function get_odd_numbers($list) {
    $odd_numbers = [];

    foreach ($list as $each) {
        if ($each % 2 === 1) {
            $odd_numbers[] = $each;
        }
    }

    return $odd_numbers;
}