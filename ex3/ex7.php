<?php

$file = 'data/grades.txt';

$additionalData = ['history' => 5, 'chemistry' => 2];


$lines = [];
foreach ($additionalData as $subject => $grade) {
    $lines[] = "$subject;$grade";
}

$string = join(PHP_EOL, $lines);

file_put_contents($file, $string, FILE_APPEND | LOCK_EX);