<?php
require_once 'AuthorDao.php';

class Book
{
    public $book_id;
    public $title;
    public $authorID;
    public $firstName;
    public $lastName;
    public $grade;
    public $isRead;

    public function __construct($book_id, $title, $authorID, $firstName, $lastName, $grade, $isRead)
    {
        $this->book_id = $book_id;
        $this->title = $title;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->authorID = $authorID;
        if ($grade != (int)$grade) $this->grade = 0;
        else {
            $this->grade = $grade;
        }
        $this->isRead = $isRead;
    }

    public function getID () {
        return $this->book_id;
    }
}