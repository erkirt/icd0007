<?php


class Author
{
    public $author_id;
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($author_id, $firstName, $lastName, $grade)
    {
        $this->author_id = $author_id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        if ($grade != (int)$grade) $this->grade = 0;
        else {
            $this->grade = $grade;}
    }
    public function getID () {
        return $this->author_id;
    }
}