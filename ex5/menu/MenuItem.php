<?php

class MenuItem {

    public int $id;
    public string $name;
    public array $subItems = [];

    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }

    public function addSubItem(MenuItem $subItem) : void {
        $this->subItems[] = $subItem;
    }
}

//function getConnection(){
//
//    $user = "erkirt";
//    $pass = "df6a2d";
//
//    $address = "mysql:host=db.mkalmo.xyz;dbname=erkirt";
//
//    return new PDO($address, $user, $pass,
//        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
//}