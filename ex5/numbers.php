<?php

require_once 'connection.php';

$conn = getConnection();

//$stmt = $conn->prepare('select num as my_number from number where num > :threshold');
//
//$stmt->bindValue(':threshold', 80);
//
//$stmt->execute();
//
//foreach ($stmt as $row){
//    var_dump($row[0]);
//}INSERT INTO contact VALUES (null, 'Alice');


$stmt = $conn->prepare('INSERT INTO contact (name) VALUE (:name)');
$stmt->bindValue(':name', 'Jill');
$stmt->execute();
$lastInsertId = $conn->lastInsertId();
$phones = ['1', '2', '3'];
$stmt = $conn->prepare(
    'insert into phone values (:contact_id, :number)');
foreach ($phones as $phone){
    $stmt->bindValue(':contact_id', $lastInsertId);
    $stmt->bindValue(':number', $phone);
    $stmt->execute();
}


